package com.desafioconcreteandroid;

import android.test.ActivityInstrumentationTestCase2;
import com.desafioconcreteandroid.ui.pullRequests.PullRequestsActivity;
import com.desafioconcreteandroid.ui.repositories.RepositoriesActivity;
import com.robotium.solo.Solo;

/**
 * Created by jsantini on 30/09/17.
 */

public class NavigationTest extends ActivityInstrumentationTestCase2<RepositoriesActivity> {

    private Solo solo;

    public NavigationTest() {
        super(RepositoriesActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

    public void testRun() {
        solo.waitForActivity("RepositoriesActivity", 2000);
        solo.assertCurrentActivity("RepositoriesActivity", RepositoriesActivity.class);
        solo.clickInRecyclerView(1);
        solo.waitForActivity("PullRequestsActivity", 2000);
        solo.assertCurrentActivity("PullRequestsActivity", PullRequestsActivity.class);
        solo.goBack();
        solo.assertCurrentActivity("RepositoriesActivity", RepositoriesActivity.class);
    }
}
