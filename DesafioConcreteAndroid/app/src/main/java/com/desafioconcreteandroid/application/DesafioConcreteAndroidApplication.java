package com.desafioconcreteandroid.application;

import android.app.Application;
import android.content.Context;

/**
 * Created by jsantini on 29/09/17.
 */

public class DesafioConcreteAndroidApplication extends Application {

    private static Context mContext;
    private static DesafioConcreteAndroidApplication mInstance;

    public static Context getAppContext() {
        return mContext;
    }

    public static synchronized DesafioConcreteAndroidApplication getInstance() {
        return mInstance;
    }

    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mInstance = this;
    }
}
