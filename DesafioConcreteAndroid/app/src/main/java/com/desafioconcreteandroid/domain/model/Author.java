package com.desafioconcreteandroid.domain.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jsantini on 29/09/17.
 */

public class Author implements Serializable {

    @SerializedName("login")
    private String authorName;

    @SerializedName("avatar_url")
    private String authorAvatar;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorAvatar() {
        return authorAvatar;
    }

    public void setAuthorAvatar(String authorAvatar) {
        this.authorAvatar = authorAvatar;
    }
}
