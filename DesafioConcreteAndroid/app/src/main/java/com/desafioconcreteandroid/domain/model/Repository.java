package com.desafioconcreteandroid.domain.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jsantini on 29/09/17.
 */

public class Repository implements Serializable {

    private long id;

    private String name;

    @SerializedName("full_name")
    private String fullName;

    private String description;

    @SerializedName("owner")
    private Author author;

    @SerializedName("stargazers_count")
    private long starsCount;

    @SerializedName("forks_count")
    private long forksCount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public long getStarsCount() {
        return starsCount;
    }

    public void setStarsCount(long starsCount) {
        this.starsCount = starsCount;
    }

    public long getForksCount() {
        return forksCount;
    }

    public void setForksCount(long forksCount) {
        this.forksCount = forksCount;
    }
}
