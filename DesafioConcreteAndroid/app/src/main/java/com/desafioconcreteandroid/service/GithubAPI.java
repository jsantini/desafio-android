package com.desafioconcreteandroid.service;

import android.content.Context;
import android.util.Log;

import com.desafioconcreteandroid.domain.dto.ApiGenericResponseDTO;
import com.desafioconcreteandroid.domain.model.PullRequest;
import com.desafioconcreteandroid.domain.model.Repository;
import com.desafioconcreteandroid.util.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by jsantini on 29/09/17.
 */

public class GithubAPI  {

    private static GithubAPI.GithubAPIContract githubAPI;
    private static final String URL_BASE = "https://api.github.com/";

    public static GithubAPI.GithubAPIContract getApi(final Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        File httpCacheDirectory = new File(context.getCacheDir(), "gitHubResponses");
        Cache cache = new Cache(httpCacheDirectory, 10 * 1024 * 1024);
        if (cache != null) {
            builder.cache(cache);
        }

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                String headerCachControl = null;
                if (NetworkUtils.isConnected()) {
                    int maxAge = 60;
                    headerCachControl = "public, max-age=" + maxAge;
                } else {
                    int maxStale = 60 * 60 * 24 * 28;
                    headerCachControl = "public, only-if-cached, max-stale=" + maxStale;
                }
                Request original = chain.request();
                Request.Builder requestBuilder = chain.request().newBuilder();
                requestBuilder.header("Content-Type", "application/json");
                requestBuilder.header("Accept", "application/json");
                requestBuilder.header("Cache-Control", headerCachControl);
                requestBuilder.method(original.method(), original.body());
                return chain.proceed(requestBuilder.build());
            }
        });

        builder.cache(cache);
        OkHttpClient okClient = builder.build();

        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy hh:mm:ss").create();
        Retrofit client = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        githubAPI = client.create(GithubAPIContract.class);
        return githubAPI;
    }

    public interface GithubAPIContract {

        @GET("search/repositories?q=language:Java&sort=stars&page=1")
        Call<ApiGenericResponseDTO<List<Repository>>> getRepositories(@Query("page") int page);

        @GET("repos/{creator}/{repository}/pulls")
        Call<List<PullRequest>> getPullRequests(@Path("creator") String creator,
                                                @Path("repository") String repository);

    }
}
