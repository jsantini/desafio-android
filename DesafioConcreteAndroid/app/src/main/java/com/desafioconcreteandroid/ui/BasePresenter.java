package com.desafioconcreteandroid.ui;

/**
 * Created by jsantini on 29/09/17.
 */

public interface BasePresenter {

    void start();

}
