package com.desafioconcreteandroid.ui;

import com.desafioconcreteandroid.domain.dto.ApiGenericResponseDTO;
import com.desafioconcreteandroid.domain.model.Repository;

import java.util.List;

import retrofit2.Response;

/**
 * Created by jsantini on 29/09/17.
 */

public abstract class UseCase {

    public interface UseCaseCallback<R> {
        void onSuccess(Response<ApiGenericResponseDTO<List<Repository>>> response);
        void onError();
        void onGenericError();
        void onConnectionError();
    }
}
