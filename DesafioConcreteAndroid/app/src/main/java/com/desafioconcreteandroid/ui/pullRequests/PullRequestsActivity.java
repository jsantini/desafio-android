package com.desafioconcreteandroid.ui.pullRequests;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.desafioconcreteandroid.R;
import com.desafioconcreteandroid.domain.model.PullRequest;
import com.desafioconcreteandroid.domain.model.Repository;
import com.desafioconcreteandroid.ui.BaseActivity;
import com.desafioconcreteandroid.ui.dialog.MessageDialog;
import com.desafioconcreteandroid.ui.pullRequests.adapter.PullRequestsAdapter;
import com.desafioconcreteandroid.ui.pullRequests.useCase.PullRequestUseCase;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PullRequestsActivity extends BaseActivity implements PullRequestsContract.View, PullRequestsAdapter.OnPullRequestClick {

    public static final String EXTRA_CREATOR = "CREATOR";
    public static final String EXTRA_REPOSITORY = "REPOSITORY";
    private static final String STATE_ITEMS = "ITEMS";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private PullRequestsContract.Presenter mPresenter;

    @BindView(R.id.rv_pull_requests)
    RecyclerView rvPullRequests;

    private PullRequestsAdapter pullRequestAdapter;

    public static final Intent getStartIntent(@NonNull final Context context,
                                              @NonNull final String creator,
                                              @NonNull final String repository) {
        Intent intent = new Intent(context, PullRequestsActivity.class);
        intent.putExtra(EXTRA_CREATOR, creator);
        intent.putExtra(EXTRA_REPOSITORY, repository);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);
        ButterKnife.bind(this);
        this.mPresenter = new PullRequestsPresenter(this,
                new PullRequestUseCase(this),
                getIntent().getStringExtra(EXTRA_CREATOR),
                getIntent().getStringExtra(EXTRA_REPOSITORY));
        setupToolbar();
        setupList();
        if (savedInstanceState != null) {
            mPresenter.setSavedState((List<PullRequest>) savedInstanceState.getSerializable(STATE_ITEMS));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_ITEMS, (Serializable) mPresenter.getItems());
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setTitle(R.string.pull_requests_title);
        ab.setDisplayHomeAsUpEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_white_48dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setupList() {
        pullRequestAdapter = new PullRequestsAdapter(this);
        rvPullRequests.setLayoutManager(new LinearLayoutManager(this));
        rvPullRequests.setAdapter(pullRequestAdapter);
        pullRequestAdapter.setmOnPullRequestClick(this);
    }

    @Override
    public void setPresenter(PullRequestsContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showLoadPullRequestsError() {
        showOrHideLoading(false, null);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MessageDialog messageDialog = MessageDialog.newInstance(
                getResources().getString(R.string.msg_load_pull_requests_error), null, MessageDialog.TYPE_ERROR);
        messageDialog.setCancelable(false);
        messageDialog.show(ft, "dialog");
    }

    @Override
    public void showPullRequests(final List<PullRequest> pullRequests) {
        pullRequestAdapter.setPullRequestList(pullRequests);
    }

    @Override
    public String getMsgLoadingPullRequests() {
        return getString(R.string.msg_loading_pull_request);
    }

    @Override
    public void onPullRequestClick(@NonNull final String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
