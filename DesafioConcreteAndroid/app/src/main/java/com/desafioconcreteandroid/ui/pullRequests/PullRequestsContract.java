package com.desafioconcreteandroid.ui.pullRequests;

import com.desafioconcreteandroid.domain.model.PullRequest;
import com.desafioconcreteandroid.domain.model.Repository;
import com.desafioconcreteandroid.ui.BasePresenter;
import com.desafioconcreteandroid.ui.BaseView;

import java.util.List;

/**
 * Created by jsantini on 29/09/17.
 */

public interface PullRequestsContract {

    interface View extends BaseView<Presenter> {

        void showLoadPullRequestsError();

        void showPullRequests(List<PullRequest> pullRequests);

        String getMsgLoadingPullRequests();
    }

    interface Presenter extends BasePresenter {

        List<PullRequest> getItems();

        void setSavedState(List<PullRequest> pullRequests);
    }
}
