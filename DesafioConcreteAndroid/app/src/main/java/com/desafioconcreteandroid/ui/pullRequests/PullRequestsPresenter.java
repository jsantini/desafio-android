package com.desafioconcreteandroid.ui.pullRequests;

import android.support.annotation.NonNull;

import com.desafioconcreteandroid.domain.dto.ApiGenericResponseDTO;
import com.desafioconcreteandroid.domain.model.PullRequest;
import com.desafioconcreteandroid.domain.model.Repository;
import com.desafioconcreteandroid.ui.UseCase;
import com.desafioconcreteandroid.ui.pullRequests.useCase.PullRequestUseCase;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Response;

/**
 * Created by jsantini on 29/09/17.
 */

public class PullRequestsPresenter implements PullRequestsContract.Presenter {

    private final PullRequestsContract.View mPullRequestView;
    private final PullRequestUseCase mPullRequestRepository;
    private final String creator;
    private final String repository;
    private List<PullRequest> pullRequests;

    public PullRequestsPresenter(@NonNull final PullRequestsContract.View view,
                                 @NonNull final PullRequestUseCase pullRequestUseCase,
                                 @NonNull final String creator,
                                 @NonNull final String repository) {

        this.mPullRequestView = view;
        this.mPullRequestRepository = pullRequestUseCase;
        this.creator = creator;
        this.repository = repository;
    }

    @Override
    public void start() {
        if(pullRequests != null) {
            mPullRequestView.showOrHideLoading(false, null);
            mPullRequestView.showPullRequests(pullRequests);
        } else {
            loadPullRequests();
        }
    }

    private void loadPullRequests() {
        mPullRequestView.showOrHideLoading(true, mPullRequestView.getMsgLoadingPullRequests());
        mPullRequestRepository.getPullRequests(creator, repository, new UseCase.UseCaseCallback() {
            @Override
            public void onSuccess(Response response) {
                pullRequests = (List<PullRequest>) response.body();
                Collections.sort(pullRequests, Collections.reverseOrder(new Comparator<PullRequest>() {
                    @Override
                    public int compare(PullRequest pr1, PullRequest pr2) {
                        return pr1.getUpdatedAt().compareTo(pr2.getUpdatedAt());
                    }
                }));
                // Verifica se view a é ser capaz de receber atualizações
                if(!mPullRequestView.isActive()) {
                    return;
                }
                mPullRequestView.showOrHideLoading(false, null);
                mPullRequestView.showPullRequests(pullRequests);
            }

            @Override
            public void onError() {
                mPullRequestView.showOrHideLoading(false, null);
                mPullRequestView.showLoadPullRequestsError();
            }

            @Override
            public void onGenericError() {
                mPullRequestView.showGenericError();
            }

            @Override
            public void onConnectionError() {
                mPullRequestView.showConnectionError();
            }
        });
    }

    @Override
    public List<PullRequest> getItems() {
        return pullRequests;
    }

    @Override
    public void setSavedState(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }
}
