package com.desafioconcreteandroid.ui.pullRequests.useCase;

import android.content.Context;
import android.support.annotation.NonNull;

import com.desafioconcreteandroid.domain.dto.ApiGenericResponseDTO;
import com.desafioconcreteandroid.domain.model.PullRequest;
import com.desafioconcreteandroid.domain.model.Repository;
import com.desafioconcreteandroid.service.GithubAPI;
import com.desafioconcreteandroid.ui.UseCase;
import com.desafioconcreteandroid.util.NetworkUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jsantini on 29/09/17.
 */

public class PullRequestUseCase {

    private final Context mContext;

    public PullRequestUseCase(@NonNull final Context context) {
        this.mContext = context;
    }

    public void getPullRequests(@NonNull final String creator,
                                @NonNull final String repository,
                                @NonNull final UseCase.UseCaseCallback useCaseCallback) {
        if(NetworkUtils.isConnected()) {
            GithubAPI.GithubAPIContract service = GithubAPI.getApi(mContext);

            Call<List<PullRequest>> call = service.getPullRequests(creator, repository);

            call.enqueue(new Callback<List<PullRequest>>() {
                @Override
                public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                    if(response.isSuccessful()) {
                        useCaseCallback.onSuccess(response);
                    } else {
                        useCaseCallback.onError();
                    }
                }

                @Override
                public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                    useCaseCallback.onGenericError();
                }
            });
        } else {
            useCaseCallback.onConnectionError();
        }
    }
}
