package com.desafioconcreteandroid.ui.pullRequests.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.desafioconcreteandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jsantini on 29/09/17.
 */

public class PullRequestsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ll_content_item_pull_request)
    public LinearLayout llContentItemPullRequest;

    @BindView(R.id.tv_item_title)
    public TextView tvItemTitle;

    @BindView(R.id.tv_item_date)
    public TextView tvItemDate;

    @BindView(R.id.iv_item_author_avatar)
    public ImageView ivItemAuthorAvatar;

    @BindView(R.id.tv_item_author_name)
    public TextView tvItemAuthorName;

    @BindView(R.id.tv_item_body)
    public TextView tvItemBody;

    public PullRequestsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
