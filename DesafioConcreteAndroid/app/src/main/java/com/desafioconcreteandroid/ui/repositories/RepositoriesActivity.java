package com.desafioconcreteandroid.ui.repositories;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.desafioconcreteandroid.R;
import com.desafioconcreteandroid.domain.model.Repository;
import com.desafioconcreteandroid.ui.BaseActivity;
import com.desafioconcreteandroid.ui.dialog.MessageDialog;
import com.desafioconcreteandroid.ui.pullRequests.PullRequestsActivity;
import com.desafioconcreteandroid.ui.repositories.adapter.RepositoriesAdapter;
import com.desafioconcreteandroid.ui.repositories.useCase.RepositoriesUseCase;
import com.desafioconcreteandroid.util.EndlessScrollListener;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepositoriesActivity extends BaseActivity implements RepositoriesAdapter.OnRepositoryClickListener, RepositoriesContract.View {

    private static final String STATE_PAGE = "PAGE";
    private static final String STATE_ITEMS = "ITEMS";
    private static final String STATE_TOTAL_ITEMS = "TOTAL_ITEMS";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_repositories)
    RecyclerView rvRepositories;

    RepositoriesAdapter repositoriesAdapter;

    RepositoriesContract.Presenter mPresenter;

    EndlessScrollListener mEndlessScrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository_list);
        ButterKnife.bind(this);
        mPresenter = new RepositoriesPresenter(this, new RepositoriesUseCase(this));
        setupToolbar();
        setupList();
        if (savedInstanceState != null) {
            mPresenter.setSavedState(savedInstanceState.getInt(STATE_PAGE),
                    savedInstanceState.getLong(STATE_TOTAL_ITEMS),
                    (List<Repository>) savedInstanceState.getSerializable(STATE_ITEMS));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(STATE_TOTAL_ITEMS, mPresenter.getTotalItems());
        outState.putInt(STATE_PAGE, mPresenter.getPage());
        outState.putSerializable(STATE_ITEMS, (Serializable) mPresenter.getItems());
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setTitle(R.string.repositories_title);
        ab.setDisplayHomeAsUpEnabled(false);
    }

    private void setupList() {
        repositoriesAdapter = new RepositoriesAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvRepositories.setLayoutManager(linearLayoutManager);
        rvRepositories.setAdapter(repositoriesAdapter);
        mEndlessScrollListener = new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                repositoriesAdapter.setIsAppending(true);
                mPresenter.loadRepositories(current_page);
            }
        };
        rvRepositories.addOnScrollListener(mEndlessScrollListener);
        repositoriesAdapter.setmOnRepositoryClickListener(this);
    }

    @Override
    public void onRepositoryClick(@NonNull final String creator, @NonNull final String repository) {
        startActivity(PullRequestsActivity.getStartIntent(this, creator, repository));
    }

    @Override
    public void setPresenter(@NonNull final RepositoriesContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public String getMsgLoadingRepositories() {
        return getString(R.string.msg_loading_repositories);
    }

    @Override
    public void showRepositories(final List<Repository> repositories, final long totalItems) {
        mEndlessScrollListener.setTotalEntries(totalItems);
        repositoriesAdapter.setRepositories(repositories);
    }

    @Override
    public void showLoadRepositoriesError() {
        showOrHideLoading(false, null);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MessageDialog messageDialog = MessageDialog.newInstance(
                getResources().getString(R.string.msg_load_repositories_error), null, MessageDialog.TYPE_ERROR);
        messageDialog.setCancelable(false);
        messageDialog.show(ft, "dialog");
    }
}
