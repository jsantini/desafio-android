package com.desafioconcreteandroid.ui.repositories;

import com.desafioconcreteandroid.domain.model.Repository;
import com.desafioconcreteandroid.ui.BasePresenter;
import com.desafioconcreteandroid.ui.BaseView;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jsantini on 29/09/17.
 */

public interface RepositoriesContract {

    interface View extends BaseView<Presenter> {


        String getMsgLoadingRepositories();

        void showRepositories(List<Repository> repositories, long totalItems);

        void showLoadRepositoriesError();
    }

    interface Presenter extends BasePresenter {

        void loadRepositories(int page);

        int getPage();

        List<Repository> getItems();

        void setSavedState(int page, long totalItems, List<Repository> repositories);

        long getTotalItems();
    }
}
