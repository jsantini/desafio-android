package com.desafioconcreteandroid.ui.repositories;

import android.support.annotation.NonNull;

import com.desafioconcreteandroid.domain.dto.ApiGenericResponseDTO;
import com.desafioconcreteandroid.domain.model.Repository;
import com.desafioconcreteandroid.ui.UseCase;
import com.desafioconcreteandroid.ui.repositories.useCase.RepositoriesUseCase;

import java.io.Serializable;
import java.util.List;

import retrofit2.Response;

/**
 * Created by jsantini on 29/09/17.
 */

public class RepositoriesPresenter implements RepositoriesContract.Presenter {

    private final RepositoriesContract.View mRepositoriesView;
    private final RepositoriesUseCase mRepositoriesUseCase;
    private int page = 1;
    private long totalItems;
    private List<Repository> repositories;

    public RepositoriesPresenter(@NonNull final RepositoriesContract.View view,
                                 @NonNull final RepositoriesUseCase repositoriesUseCase) {
        this.mRepositoriesView = view;
        this.mRepositoriesUseCase = repositoriesUseCase;
    }

    @Override
    public void start() {
        if(repositories != null) {
            mRepositoriesView.showOrHideLoading(false, null);
            mRepositoriesView.showRepositories(repositories, totalItems);
        } else {
            loadRepositories(page);
        }
    }

    @Override
    public void loadRepositories(final int page) {
        this.page = page;
        mRepositoriesView.showOrHideLoading(true, mRepositoriesView.getMsgLoadingRepositories());
        mRepositoriesUseCase.getRepositories(page, new UseCase.UseCaseCallback() {
            @Override
            public void onSuccess(Response response) {
                ApiGenericResponseDTO<List<Repository>> apiResponse = (ApiGenericResponseDTO<List<Repository>>) response.body();
                repositories = apiResponse.getItems();
                if(!mRepositoriesView.isActive()) {
                    return;
                }
                totalItems = apiResponse.getTotalCount();
                mRepositoriesView.showOrHideLoading(false, null);
                mRepositoriesView.showRepositories(repositories, totalItems);
            }

            @Override
            public void onError() {
                mRepositoriesView.showLoadRepositoriesError();
            }

            @Override
            public void onGenericError() {
                mRepositoriesView.showGenericError();
            }

            @Override
            public void onConnectionError() {
                mRepositoriesView.showConnectionError();
            }
        });
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public List<Repository> getItems() {
        return repositories;
    }

    @Override
    public void setSavedState(final int page, final long totalItems, final List<Repository> repositories) {
        this.page = page;
        this.totalItems = totalItems;
        this.repositories = repositories;
    }

    @Override
    public long getTotalItems() {
        return totalItems;
    }
}
