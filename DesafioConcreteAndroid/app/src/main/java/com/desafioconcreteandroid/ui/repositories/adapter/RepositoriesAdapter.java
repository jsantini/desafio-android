package com.desafioconcreteandroid.ui.repositories.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.desafioconcreteandroid.R;
import com.desafioconcreteandroid.domain.model.Repository;
import com.desafioconcreteandroid.ui.repositories.viewHolder.RepositoriesViewHolder;
import com.desafioconcreteandroid.util.CircleTransformUtil;
import com.desafioconcreteandroid.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jsantini on 29/09/17.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesViewHolder> {

    private final Context mContext;
    private List<Repository> repositories;
    private OnRepositoryClickListener mOnRepositoryClickListener;
    private boolean isAppending;

    public RepositoriesAdapter(@NonNull final Context context) {
        mContext = context;
    }

    @Override
    public RepositoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_repository, parent, false);
        return new RepositoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepositoriesViewHolder holder, final int position) {

        final Repository repository = repositories.get(position);

        if(!Utils.stringIsEmpty(repository.getName())) {
            holder.tvItemName.setText(repository.getName());
        }

        if(!Utils.stringIsEmpty(repository.getDescription())) {
            String description = repository.getDescription().trim();
            if(description.length() > 90) {
                description = description.substring(0,90) + "...";
            }
            holder.tvItemDescription.setText(description);
        }

        Picasso.with(mContext)
                .load(repository.getAuthor().getAuthorAvatar())
                .placeholder(R.drawable.noimage)
                .transform(new CircleTransformUtil())
                .into(holder.ivItemAuthorAvatar);

        if(!Utils.stringIsEmpty(repository.getAuthor().getAuthorName())) {
            holder.tvItemAuthorName.setText(repository.getAuthor().getAuthorName());
        }

        holder.tvItemForksCount.setText(repository.getForksCount() +"");
        holder.tvItemStarsCount.setText(repository.getStarsCount() +"");

        if(mOnRepositoryClickListener != null) {
            holder.llContentItemRepository.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnRepositoryClickListener.onRepositoryClick(
                            repository.getAuthor().getAuthorName(),
                            repository.getName());
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if(repositories == null) {
            return 0;
        }
        return repositories.size();
    }

    public void setIsAppending(boolean isAppending) {
        this.isAppending = isAppending;
    }

    public interface OnRepositoryClickListener {
        void onRepositoryClick(@NonNull String creator, @NonNull String repository);
    }

    public void setmOnRepositoryClickListener(@NonNull final OnRepositoryClickListener onRepositoryClickListener) {
        mOnRepositoryClickListener = onRepositoryClickListener;
    }

    public void setRepositories(final List<Repository> repositories) {
        if(isAppending) {
            this.repositories.addAll(repositories);
            notifyItemInserted(getItemCount());
        } else {
            this.repositories = repositories;
            notifyDataSetChanged();
        }
    }

}
