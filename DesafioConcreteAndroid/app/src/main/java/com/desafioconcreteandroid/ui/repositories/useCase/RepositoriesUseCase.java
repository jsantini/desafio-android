package com.desafioconcreteandroid.ui.repositories.useCase;

import android.content.Context;
import android.support.annotation.NonNull;

import com.desafioconcreteandroid.domain.dto.ApiGenericResponseDTO;
import com.desafioconcreteandroid.domain.model.Repository;
import com.desafioconcreteandroid.service.GithubAPI;
import com.desafioconcreteandroid.ui.UseCase;
import com.desafioconcreteandroid.util.NetworkUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jsantini on 29/09/17.
 */

public class RepositoriesUseCase {

    private final Context mContext;

    public RepositoriesUseCase(@NonNull final Context context) {
        this.mContext = context;
    }

    public void getRepositories(final int page, final UseCase.UseCaseCallback useCaseCallback) {
        if(NetworkUtils.isConnected()) {
            GithubAPI.GithubAPIContract service = GithubAPI.getApi(mContext);

            Call<ApiGenericResponseDTO<List<Repository>>> call = service.getRepositories(page);

            call.enqueue(new Callback<ApiGenericResponseDTO<List<Repository>>>() {
                @Override
                public void onResponse(Call<ApiGenericResponseDTO<List<Repository>>> call, Response<ApiGenericResponseDTO<List<Repository>>> response) {
                    if(response.isSuccessful()) {
                        useCaseCallback.onSuccess(response);
                    } else {
                        useCaseCallback.onError();
                    }
                }

                @Override
                public void onFailure(Call<ApiGenericResponseDTO<List<Repository>>> call, Throwable t) {
                    useCaseCallback.onGenericError();
                }
            });
        } else {
            useCaseCallback.onConnectionError();
        }
    }
}
