package com.desafioconcreteandroid.util;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by jsantini on 30/09/17.
 */

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {
    private boolean mLoading = false;

    private int previousItemCount = 0;

    private long mTotalEntries;
    private int current_page = 1;

    private LinearLayoutManager mLinearLayoutManager;

    public EndlessScrollListener(LinearLayoutManager linearLayoutManager) {
        mLinearLayoutManager = linearLayoutManager;
    }

    public abstract void onLoadMore(int currentPage);

    public void setTotalEntries(long totalEntries) {
        mTotalEntries = totalEntries;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = mLinearLayoutManager.getItemCount();
        int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        if (mLoading) {
            int diffCurrentFromPrevious = totalItemCount - previousItemCount;

            if ((diffCurrentFromPrevious > 1) ||
                    totalItemCount >= mTotalEntries) {
                mLoading = false;
                previousItemCount = totalItemCount;
            }
        } else {
            if (totalItemCount < mTotalEntries) {
                if ((firstVisibleItem + visibleItemCount) >= totalItemCount &&
                        totalItemCount < mTotalEntries) {
                    onLoadMore(++current_page);

                    mLoading = true;
                    previousItemCount = totalItemCount;
                }
            }
        }
    }
}
