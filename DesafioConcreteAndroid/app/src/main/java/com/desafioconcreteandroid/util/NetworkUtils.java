package com.desafioconcreteandroid.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.desafioconcreteandroid.application.DesafioConcreteAndroidApplication;

/**
 * Created by jsantini on 29/09/17.
 */

public class NetworkUtils {

    /**
     * Checks whether there's an active internet connection at the current moment.
     * @return TRUE if there's internet, FALSE if you're offline.
     *
     */
    public static boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) DesafioConcreteAndroidApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
